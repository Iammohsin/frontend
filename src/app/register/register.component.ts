import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import {Register} from '../register';
import {Observable} from 'rxjs';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  dataSaved = false;
  employeeForm: any;
  massage:string;
  constructor(private formbulider: FormBuilder,private loginService:LoginService) { }

  ngOnInit() {
    this.employeeForm = this.formbulider.group({
      UserName: ['', [Validators.required]],
      LoginName: ['', [Validators.required]],
      Password: ['', [Validators.required]],
      Email: ['', [Validators.required]],
      ContactNo: ['', [Validators.required]],
      Address: ['', [Validators.required]],
    });
  }
   onFormSubmit()
  {
    const employee = this.employeeForm.value;
    this.Createemployee(employee);
  }
  Createemployee(register:Register)
  {
  this.loginService.CreateUser(register).subscribe(
    ()=>
    {
      this.dataSaved = true;
      this.massage = 'Record saved Successfully';
      this.employeeForm.reset();
    });
  }
}
