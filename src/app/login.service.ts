import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { Register } from "../app/register";
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  rootPath :string;
  token : string;
  header : any;
  constructor(private http : HttpClient) { 

    this.rootPath = 'http://localhost:5400/api/Login/';

   
    const headerSettings: {[name: string]: string | string[]; } = {};
  //  headerSettings['Authorization'] = 'Bearer ' + this.token;
    //headerSettings['Content-Type'] = 'application/json';
    this.header = new HttpHeaders(headerSettings);
  }
  Login(model : any){
    debugger;
     var a =this.rootPath+'UserLogin';
   return this.http.post<any>(this.rootPath+'UserLogin',model,{ headers: this.header});
  }
  // TokenValidate(TokenModel : any){
  //   debugger;
  //  return this.http.get<any>(this.rootPath+'Validate?token='+TokenModel.tokenResponse+'&username='+TokenModel.UserName,{ headers: this.header});
  // }
   CreateUser(register:Register)
   {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post<Register[]>(this.rootPath + '/createcontact/', register, httpOptions)
   }
}
