import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';
 import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  model : any={};
  // TokenModel : any={'UserName':'','tokenResponse':''};
  // tokenResponse:string;
  errorMessage:string;
  constructor(private router:Router,private LoginService:LoginService) { }


  ngOnInit() {
    sessionStorage.removeItem('UserName');
    sessionStorage.clear();
  }
  login(){
    debugger;
    this.LoginService.Login(this.model).subscribe(
      data => {
        debugger;
        if(data.Status=="Success")
        {
          // alert("Ok");
          this.router.navigate(['/Dasboard']);
          // this.router.navigate(['/dashboard']);
          debugger;
          // this.TokenModel.UserName=this.model.UserName;
          // this.TokenModel.tokenResponse=data.Message;
          // this.LoginService.Login(this.TokenModel).subscribe(
          //   response => {
          //     debugger;
          //     if(response.Status=="Success")
          //     {
          //       this.router.navigate(['/dashboard']);
          //     }
          //     else{
          //       this.errorMessage = response.Message;
          //     }
          //   },
          //   error => {
          //     this.errorMessage = error.message;
          //   });
        }
        else{
          this.errorMessage = data.Message;
        }
      },
      error => {
        this.errorMessage = error.message;
      });
  };
 }

